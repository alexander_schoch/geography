import React from 'react';
import axios from 'axios';

import {
  Avatar,
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Container,
  Grid,
  InputAdornment,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  TextField,
  Tooltip,
  Typography,
} from '@mui/material';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

import {
  Send,
} from '@mui/icons-material';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      message: '',
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.scrollToBottom = this.scrollToBottom.bind(this);
  }

  componentDidMount() {
    //this.scrollToBottom();
  }

  componentDidUpdate() {
    //this.scrollToBottom();
  }

  handleSubmit(e) {
    e.preventDefault();
    if(this.state.message != '') {
      axios.post('/api/messages', {
        game: '/api/games/' + this.props.game.id,
        player: '/api/players/' + this.props.player.id,
        text: this.state.message,
      }).then(() => {
        this.setState({message: ''});
      });
    }
  }

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  }

  render() {
    return (
      <Card style={{ marginTop: '50px' }} variant="outlined">
        <CardHeader title="Chat"/>

        <CardContent>
          <Box style={{ maxHeight: '300px', overflow: 'scroll' }}>
            <List sx={{ width: '100%' }}>
              {this.props.game.messages.map(m => {
                return this.props.player.id == m.player.id ?
                  <ListItem key={m.id}>
                    <ListItemText primary={<Typography sx={{ textAlign: 'right' }}>{m.text}</Typography>} secondary={<Typography sx={{ textAlign: 'right' }} color="text.secondary">{m.player.name}</Typography>} style={{ paddingRight: '20px' }} />
                    <ListItemAvatar>
                      <Avatar sx={{ bgcolor: m.player.color, mr: 2, my: 0.5 }}>{m.player.name[0]}</Avatar>
                    </ListItemAvatar>
                  </ListItem> :
                  <ListItem key={m.id}>
                    <ListItemAvatar>
                      <Avatar sx={{ bgcolor: m.player.color, mr: 2, my: 0.5 }}>{m.player.name[0]}</Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={m.text} secondary={m.player.name} />
                  </ListItem>
              })}
            </List>
            <div style={{ float:"left", clear: "both" }}
              ref={(el) => { this.messagesEnd = el; }}>
            </div>
          </Box>
          <Box>
            <form onSubmit={this.handleSubmit}>
              <Box sx={{ display: 'flex' }}>
                <TextField
                  label="Message"
                  value={this.state.message}
                  onChange={(e) => {this.setState({message: e.target.value})}}
                  fullWidth
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <Button type="submit" color="primary" component="label" fullWidth style={{ height: "100%" }} endIcon={<Send/>} onClick={(e) => this.handleSubmit(e)}>
                          Send
                        </Button>
                      </InputAdornment>
                    ),
                  }}
                />
              </Box>
            </form>
          </Box>
        </CardContent>
      </Card>
    );
  }
}

export default Chat;
