import React from 'react';
import * as ReactDOM from 'react-dom/client';
import axios from 'axios';

import {
  Alert,
  Avatar,
  AvatarGroup,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Chip,
  Container,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  Divider,
  Grid,
  InputAdornment,
  Icon,
  LinearProgress,
  TextField,
  ToggleButton,
  ToggleButtonGroup,
  Tooltip,
  Typography,
} from '@mui/material';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

import CheckIcon from '@mui/icons-material/Check';
import ClearIcon from '@mui/icons-material/Clear';

import Chat from './chat';

function CrownIcon(props) {
  return (
    <Icon style={{ textAlign: 'center' }}>
      <img style={{ width: '100%' }} src='/crown.svg' />
    </Icon>
  );
}

class Discuss extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      game: null,
      player: null,
      round: null,
      progress: -1,
      alreadyCalculatedPoints: false,
      allowContinue: false,
      done: false,
    }

    this.getGame = this.getGame.bind(this);
    this.getPlayer = this.getPlayer.bind(this);
    this.getRound = this.getRound.bind(this);
    this.updateField = this.updateField.bind(this);
    this.isApproved = this.isApproved.bind(this);
    this.toggleApprove = this.toggleApprove.bind(this);
    this.findPlayer = this.findPlayer.bind(this);
    this.isEveryoneReady = this.isEveryoneReady.bind(this);
    this.getMaxPoints = this.getMaxPoints.bind(this);
    this.getMinPoints = this.getMinPoints.bind(this);
    this.nextRound = this.nextRound.bind(this);
    this.getRandomLetter = this.getRandomLetter.bind(this);
    this.goToNextRound = this.goToNextRound.bind(this);
    this.calculatePoints = this.calculatePoints.bind(this);
  }

  componentDidMount() {
    const promise1 = this.getGame();
    const promise2 = this.getPlayer();
    Promise.all([promise1, promise2]).then(() => {
      this.getRound();
      setInterval(this.getRound, 3000);
      setInterval(this.getGame, 6000);
      setInterval(this.goToNextRound, 6000);
      setInterval(this.calculatePoints, 3000);
    });
  }

  async getPlayer() {
    const response = await axios.get('/api/user');
    if(response.data == '') {
      window.location = '/lobby/' + this.state.game.id;
    } else {
      const response2 = await axios.get('/api/players/' + response.data)
      return new Promise((resolve) => {
        this.setState({player: response2.data}, () => {
          resolve();
        });
      });
    }
  }

  async getGame() {
    let split = window.location.href.split('/');
    const id = split[split.length-1];
    const response = await axios.get('/api/games/' + id);
    return new Promise((resolve) => {
      this.setState({
        game: response.data,
        progress: this.state.progress == -1 ? response.data.progress : this.state.progress,
      }, () => {
        resolve();
      });
    });
  }

  async getRound() {
    const response = await axios.get('/api/rounds/' + this.state.game.rounds[this.state.progress].id);
    return new Promise((resolve) => {
      this.setState({
        round: response.data
      }, () => {
        resolve();
      });
    });
  }

  getAnswers(field) { // returns an array with all answers from this field/round with player information
    let obj = [];
    this.state.game.players.map(player => {
      obj.push({'player': player, 'answer': this.state.round.answers.filter(a => a.player == '/api/players/' + player.id && a.field == '/api/fields/' + field.id)[0]});
    });
    return obj;
  }

  updateField(answer, e) {
    axios.patch(answer.answer['@id'], {
      answer: e.target.value,
    }, {
      headers: {
        'Content-Type': 'application/merge-patch+json',
      }
    }).then(() => {
      this.getRound();
    });
  }

  isApproved(answer) {
    let result = true;
    answer.answer.disapprovals.map(da => {
      if(da.player == this.state.player['@id']) {
        result = false;
      }
    });
    return result;
  }

  toggleApprove(answer, current, n) {
    if(n == false && current == true) { // disapprove
      axios.post('/api/disapprovals', {
        player: this.state.player['@id'],
        answer: answer.answer['@id']
      }).then(() => {
        this.getRound();
      });
    } else if (n == true && current == false) { // approve
      let id = answer.answer.disapprovals.filter(d => d.player == this.state.player['@id'])[0]['@id'].split('/')[3];
      axios.delete('/api/disapprovals/' + id, {
        player: this.state.player['@id'],
        answer: answer.answer['@id']
      }).then(() => {
        this.getRound();
      });
    }
  }

  findPlayer(da) {
    let pl = null;
    this.state.game.players.map(p => {
      if(p['@id'] == da.player) {
        pl = p;
      }
    });
    return pl;
  }

  isEveryoneReady() {
    let result = true;
    this.state.game.players.map(p => {
      if(!p.isReady) {
        result = false;
      }
    });
    return result;
  }

  getMaxPoints() {
    let max = 0;
    this.state.game.players.map(p => {
      if(p.points > max) {
        max = p.points;
      }
    });
    return max;
  }

  getMinPoints() {
    let min = 10000;
    this.state.game.players.map(p => {
      if(p.points < min) {
        min = p.points;
      }
    });
    return min;
  }

  nextRound() {
    // set isReady to 0 for all players
    this.state.game.players.map(p => {
      axios.patch(p['@id'], {
        isReady: false,
      }, {headers: {'Content-Type': 'application/merge-patch+json'}});
    });

    // generate a new round with new letter
    axios.post('/api/rounds', {
      letter: this.getRandomLetter(),
      game: '/api/games/' + this.state.game.id,
      isFinished: false,
      isNextRound: false,
    });

    // set progress += 1 in game and forward if all rounds are over
    axios.patch(this.state.game['@id'], {
      progress: this.state.game.progress + 1,
    }, {headers: {'Content-Type': 'application/merge-patch+json'}});

    // tell players to move to the next round
    axios.patch(this.state.round['@id'], {
      isNextRound: true,
    }, {headers: {'Content-Type': 'application/merge-patch+json'}});
  }

  getRandomLetter() {
    let choices = this.state.game.characterSet;
    this.state.game.rounds.map(r => {
      choices = choices.replace(r.letter, '');
    });
    let index = Math.floor(Math.random() * choices.length);
    return choices[index];
  }

  goToNextRound() {
    if(this.state.round.isNextRound) {
      if(this.state.progress + 1 == this.state.game.numRounds) {
        window.location = '/';
      } else {
        window.location = '/game/' + this.state.game.id;
      }
    }
  }

  async calculatePoints() {
    if(this.state.game.host.id == this.state.player.id && this.isEveryoneReady() && !this.state.alreadyCalculatedPoints) {
      this.setState({alreadyCalculatedPoints: true});
      await this.state.game.fields.map(async f => {
        let answers = this.getAnswers(f);
        await answers.map(async a => {
          if(a.answer.answer.length < 2 || a.answer.disapprovals.length >= this.state.game.players.length / 2) { // no answer = 0 points, too many disapprovals = 0 points
            return;
          }
          let alreadyExisted = false;
          let noneExisted = true;
          answers.map(a2 => {
            if(a.answer.answer == a2.answer.answer && a.answer.player != a2.answer.player) { // one point
              alreadyExisted = true;
            } 
            if(a2.answer.answer.length > 1 && a.answer.player != a2.answer.player) { // 3 points
              noneExisted = false;
            }
          });
          let points = 0;
          if(alreadyExisted) {
            points = 1;
          } else if (noneExisted) {
            points = 3;
          } else {
            points = 2;
          }

          await axios.post('/api/addPoints/' + a.player.id, {
            points: points,
          })
        });
      });
      this.getGame();
      this.setState({allowContinue: true});
    }
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        {(this.state.round != null && !this.state.round.isNextRound) && <Container>
          <Dialog 
            fullWidth
            open={this.isEveryoneReady()}
          >
            <DialogTitle>Results</DialogTitle>
            <DialogContent>
              {this.state.game.players.map(p => {
                let max = this.getMaxPoints();
                let min = this.getMinPoints();
                let progress = (p.points-min) / (max-min) * 100;
                if(max == 0) {
                  return (
                    <Box key={p['@id']} sx={{ display: 'flex', alignItems: 'flex-end' }}>
                      <Grid container spacing={2}>
                        <Grid item xs={2}>
                          <Tooltip title={p.name}>
                            <Avatar sx={{ bgcolor: p.color, mr: 2, my: 0.5 }}>{p.name[0]}</Avatar>
                          </Tooltip>
                        </Grid>
                        <Grid item xs={1}>
                          <Typography>
                            {p.points}
                          </Typography>
                        </Grid>
                        <Grid item xs={9}>
                          <LinearProgress variant="buffer" value={100} valueBuffer={100} sx={{ my: '20px' }}/>
                        </Grid>
                      </Grid>
                    </Box>
                  );
                } else {
                  return (
                    <Box key={p['@id']} sx={{ display: 'flex', alignItems: 'flex-end' }}>
                      <Grid container spacing={3}>
                        <Grid item xs={3} style={{ display: 'flex' }}>
                          <Tooltip title={p.name}>
                            <Avatar sx={{ bgcolor: p.color, mr: 1, my: 0.5 }}>{progress == 100 ? <CrownIcon color="white"/> : p.name[0]}</Avatar>
                          </Tooltip>
                          <Chip label={p.points} variant="outlined" sx={{ my: 1 }}/>
                        </Grid>
                        <Grid item xs={9}>
                          <LinearProgress variant="buffer" value={progress} valueBuffer={progress} sx={{ my: '20px' }}/>
                        </Grid>
                      </Grid>
                    </Box>
                  );
                }
              })}
            </DialogContent>
            {this.state.game.host.id == this.state.player.id &&
            <DialogActions>
              <Button
                onClick={this.nextRound}
                disabled={!this.state.allowContinue}
              >
                Continue
              </Button>
            </DialogActions>}
          </Dialog>
          <Typography variant="h3" style={{ marginTop: '50px' }}>Round {this.state.progress + 1} / {this.state.game.numRounds} - Review</Typography>
          <Divider style={{ marginTop: '30px', marginBottom: '30px' }}>
            <Chip color="primary" variant="outlined" label={<span style={{ fontSize: '15pt' }}>Letter: <b>{this.state.round.letter}</b></span>} />
          </Divider>

          {this.state.game.fields.map(field => (
            <Card key={field.id} variant="outlined" style={{ marginBottom: '20px' }}>
              <CardHeader title={field.name}/>
              <CardContent>
                <Grid container spacing={2}>
                  {this.getAnswers(field).map(answer => (
                    <Grid item xs={12} key={answer.player.id}>
                      {answer.answer != null &&
                      <Grid container spacing={2}>
                        <Grid item xs={12} md={6}>
                          <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
                            <Avatar sx={{ bgcolor: answer.player.color, mr: 2, my: 0.5 }}>{answer.player.name[0]}</Avatar>
                            <TextField
                              variant="standard"
                              value={answer.answer.answer}
                              fullWidth
                              label={answer.player.name}
                              onChange={(e) => this.updateField(answer, e)}
                              disabled={answer.answer.answer.length < 2 || answer.answer.disapprovals.length >= this.state.game.players.length / 2}
                              focused
                              sx={{
                                '& .MuiInput-underline:before': { borderBottomColor: answer.player.color },
                                '& .MuiInput-underline:after': { borderBottomColor: answer.player.color },
                              }}
                              InputLabelProps={{style : {color : answer.player.color} }}
                            />
                          </Box>
                        </Grid>
                        <Grid item xs={6} md={3}>
                          {(answer.player.id != this.state.player.id && answer.answer.answer.length > 1) && <ToggleButtonGroup
                            exclusive
                            aria-label="text alignment"
                            variant="contained" 
                            value={this.isApproved(answer)}
                            onChange={(e, n) => {this.toggleApprove(answer, this.isApproved(answer), n)}}
                          >
                            <ToggleButton value={true} aria-label="yes" color="success">
                              <CheckIcon />
                            </ToggleButton>
                            <ToggleButton value={false} aria-label="no" color="error">
                              <ClearIcon />
                            </ToggleButton>
                          </ToggleButtonGroup>}
                        </Grid>
                        <Grid item xs={6} md={3}>
                          <AvatarGroup max={6} spacing={30}>
                            {answer.answer.disapprovals.map(da => {
                              let player = this.findPlayer(da);
                              return (
                                <Tooltip title={player.name} key={player['@id']}>
                                  <Avatar sx={{ bgcolor: player.color, mr: 2, my: 0.5 }}>{player.name[0]}</Avatar>
                                </Tooltip>
                              );
                            })}
                          </AvatarGroup>
                        </Grid>
                      </Grid>}
                    </Grid>
                  ))}
                </Grid>
              </CardContent>
            </Card>
          ))}
          {this.state.done ?
          <Alert severity="info">
            <Typography>
              Please wait for the others to finish review
            </Typography>
          </Alert> :
          <Box style={{ textAlign: 'center' }}>
            <Button
              size="large" 
              variant="contained"
              onClick={() => {this.setState({done: true}); axios.patch(this.state.player['@id'], {isReady: true}, {headers: {'Content-Type': 'application/merge-patch+json'}}).then(() => {this.getGame()})}}
            >
              I'm Done
            </Button>
          </Box>}
          <Box style={{ textAlign: 'center' }}>
            <Box display="flex" justifyContent="center">
            <Typography sx={{ my: '15px', mr: '10px' }}>Waiting for</Typography>
            <AvatarGroup max={6} spacing={30}>
              {this.state.game.players.filter(p => !p.isReady).map(p => {
                return (
                  <Tooltip title={p.name} key={p['@id']}>
                    <Avatar sx={{ bgcolor: p.color, mr: 2, my: 0.5 }}>{p.name[0]}</Avatar>
                  </Tooltip>
                );
              })}
            </AvatarGroup>
            </Box>
          </Box>
          {(this.state.game != null && this.state.player != null) && <Chat game={this.state.game} player={this.state.player}/>}
        </Container>}
			</ThemeProvider>
    );
  }
}

const container = document.getElementById('discuss');
const root = ReactDOM.createRoot(container);
root.render(<Discuss/>);
