import React from 'react';
import * as ReactDOM from 'react-dom/client';

import axios from 'axios';

import {
  Box,
  Button,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from '@mui/material';

import AskName from './AskName';
import MainTeaser from './mainTeaser';
import MainDescription from './mainDescription';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

class Main extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dialogOpen: false,
      name: '',
      games: [],
    }

    this.startGame = this.startGame.bind(this);
    this.getUser = this.getUser.bind(this);
    this.getGames = this.getGames.bind(this);
    this.formatFields = this.formatFields.bind(this);
    this.printPlayers = this.printPlayers.bind(this);
  }

  componentDidMount() {
    this.getGames();
    setInterval(this.getGames, 6000);
  }

  getUser() {
    return new Promise(resolve => {
      axios.get('/api/user').then(response => {
        axios.get('/api/players/' + response.data).then(response2 => {
          resolve('/api/players/' + response2.data.id);
        });
      });
    });
  }

  async startGame(name, id) {
    const user = await this.getUser();
    axios.post('/api/games', {
      host: user,
      isStarted: false,
      numRounds: 10,
      progress: 0,
      characterSet: 'ABCDEFGHIJKLMNOPRSTUVWZ',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      }
    }).then(response => {
      const game = response.data;
      axios.patch(user, {
        game: '/api/games/' + game.id,
      }, {
        headers: {
          'Content-Type': 'application/merge-patch+json',
        }
      }).then(response2 => {
        window.location = '/lobby/' + game.id;
      });
    });
  }

  async getGames() {
    const response = await fetch('/api/games', {
      headers: {
        'accept': 'application/json'
      }
    });
    const data = await response.json();
    this.setState({
      games: data,
    });
  }

  formatFields(fields) {
    var f = '';
    fields.map((field) => {
      f += field.name + ' ∙ ';
    });
    return f.substring(0, f.length - 2);
  }

  printPlayers(game) {
    let p = '';
    game.players.map(player => {
      p += player.name + ' ∙ '; 
    });
    return p.substring(0, p.length - 3);
  }

  render() {
  console.log(this.state.games);

    return (
      <ThemeProvider theme={theme}>
        <AskName dialogOpen={this.state.dialogOpen} buttonText="Start Game" callback={this.startGame} setDialogOpen={(v) => {this.setState({dialogOpen: v})}} allowClose={true}/>
        <MainTeaser/>
        <MainDescription/>
        <Container>
          <Box
            style={{ textAlign: 'center' }}
          >
            <Button
              variant="contained"
              onClick={() => {this.setState({dialogOpen: true})}}
              size="large"
              style={{ margin: '50px' }}
            >
              Start New Game
            </Button>
          </Box>
         <Typography variant="h4">Open Games</Typography>
          <TableContainer component={Paper} style={{ marginTop: '20px' }}>
            <Table sx={{ minWidth: 650 }}>
              <TableHead>
                <TableRow>
                  <TableCell><b>ID</b></TableCell>
                  <TableCell><b>Fields</b></TableCell>
                  <TableCell><b>Host</b></TableCell>
                  <TableCell><b>Players</b></TableCell>
                  <TableCell></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.games.map((game) => {
                  if(!game.isStarted) return(<TableRow key={game.id}>
                    <TableCell>{game.id}</TableCell>
                    <TableCell>{this.formatFields(game.fields)}</TableCell>
                    <TableCell>{game.host.name}</TableCell>
                    <TableCell>{this.printPlayers(game)}</TableCell>
                    <TableCell align="right"><Button component="a" href={"/lobby/" + game.id}>Join Game</Button></TableCell>
                  </TableRow>);
                })}
              </TableBody>
            </Table>
          </TableContainer>

         <Typography variant="h4" style={{ marginTop: '50px' }}>Past Games</Typography>
          <TableContainer component={Paper} style={{ marginTop: '20px' }}>
            <Table sx={{ minWidth: 650 }}>
              <TableHead>
                <TableRow>
                  <TableCell><b>Fields</b></TableCell>
                  <TableCell><b>Host</b></TableCell>
                  <TableCell><b>Players</b></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.games.map((game) => {
                  if(game.isStarted) return( <TableRow key={game.id}>
                    <TableCell>{this.formatFields(game.fields)}</TableCell>
                    <TableCell>{game.host.name}</TableCell>
                    <TableCell>{this.printPlayers(game)}</TableCell>
                  </TableRow> );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Container>
			</ThemeProvider>
    );
  }
}

const container = document.getElementById('main');
const root = ReactDOM.createRoot(container);
root.render(<Main/>);
