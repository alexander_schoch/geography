import React from 'react';
import axios from 'axios';

import {
  Avatar,
  Box,
  Card,
  CardActions,
  CardHeader,
  CardContent,
  Container,
  Grid,
  Typography,
} from '@mui/material';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

class MainTeaser extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      answers: [],
    }

    this.getAnswers = this.getAnswers.bind(this);
  }

  componentDidMount() {
    this.getAnswers();
  }

  async getAnswers() {
    // 900: 3 -> 2
    // 600: 2 -> 1
    let num = 3;
    if(window.innerWidth < 900) 
      num = 2;
    if(window.innerWidth < 600)
      num = 1;

    let arr = [];
    for(let i = 0; i < num; i++) {
      const response = await axios.get('/api/randomAnswer'); 
      const id = response.data;
      if(id > 0) {
        const response2 = await axios.get('/api/answers/' + id);
        arr.push(response2.data);
      } else {
        arr.push({
          answer: 'Wow, such Empty',
          field: {
            name: 'None',
          },
          player: {
            name: 'Nobody',
            color: '#000000',
          }
        });
      }
    }
    this.setState({answers: arr});
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <Box bgcolor="primary.main">
          <Container style={{ paddingBottom: '20px', paddingTop: '20px' }}>
            <Grid container spacing={2} alignItems="stretch">
              {this.state.answers.map((answer, i) => (
                <Grid item xs={12} sm={6} md={4} key={i} style={{ display: 'flex' }}>
                  <Card style={{display: 'flex', justifyContent: 'space-between', flexDirection: 'column', width: '100%'}}>
                    <CardContent>
                      <Typography variant="h6" style={{ marginBottom: '20px' }}>
                        <Typography component="span" color="primary.dark" variant="h6"><b>{answer.field.name}</b></Typography> with <Typography component="span" color="primary.dark" variant="h6"><b>{answer.answer.charAt(0).toUpperCase()}</b></Typography>?
                      </Typography>
                      <Typography component="span" color="primary.dark" variant="h4"><b>
                        {answer.answer}
                      </b></Typography>
                    </CardContent>
                    <CardActions>
                      <Box sx={{ display: 'flex', marginLeft: '10px', marginBottom: '10px' }}>
                        <Avatar sx={{ bgcolor: answer.player.color, mr: 2 }}>{answer.player.name.slice(0, 1)}</Avatar>
                        <Typography sx={{ my: 1.2 }}>{answer.player.name}</Typography>
                      </Box>
                    </CardActions>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </Container>
        </Box>
      </ThemeProvider>
    )
  }
}

export default MainTeaser;
