import {createTheme, ThemeProvider} from '@mui/material/styles';

const fonts = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
});

const theme = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
  palette: {
    type: 'light',
		primary: {
      main: '#B67233',
      light: '#FFFFFF',
      contrastText: '#FFFFFF',
      dark: '#B67233',
    },
    secondary: {
      main: '#099b41',
      light: '#099b41',
      dark: '#099b41',
    },
  },
});

export default theme;
