import React from 'react';

import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Container,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Grid,
  TextField,
  Typography,
} from '@mui/material';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

class IntroDialog extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
     <Dialog
        open={this.props.openDialog}
        onClose={this.props.close}
      >
        <DialogTitle>
          How this works
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            When clicking on "Start new Game", you're prompted to enter your name. This name will persist throughout this round. After clicking "Start Game", you'll enter the newly created lobby. The game you just created will also appear on the "Open Games" section on this page.
          </DialogContentText>
          <DialogContentText style={{ marginTop: '10px' }}>
            Because you opened up this new game, you're automatically assigned the "host". This means that you are the only player that can enter categories, kick players and tell the game to move to the next round.
          </DialogContentText>
          <DialogContentText variant="h8" color="black" style={{ marginTop: '10px' }}>
            The Lobby
          </DialogContentText>
          <DialogContentText style={{ marginTop: '10px' }}>
            In the "Categories" field, you can enter and remove categories. Players will have to think of answers for each category.
          </DialogContentText>
          <DialogContentText style={{ marginTop: '10px' }}>
            In the "Players" field, you'll see every player that has joined your lobby. You can also copy the invite link to send to your friends. When clicking this link or clicking "Join Game" on the main page, every new player will be promted to enter a name.
          </DialogContentText>
          <DialogContentText style={{ marginTop: '10px' }}>
            As soon as all the categories are added and all players have joined, click "Everybody's In!". This will start the game for everyone.
          </DialogContentText>
          <DialogContentText variant="h8" color="black" style={{ marginTop: '10px' }}>
            The Game
          </DialogContentText>
          <DialogContentText style={{ marginTop: '10px' }}>
            On the top, you'll see a chip stating e.g. "Letter: O". Now, try to find an example starting with this letter for each category. You'll quickly notice that it is only possible to enter something that does actually start with this letter.
          </DialogContentText>
          <DialogContentText style={{ marginTop: '10px' }}>
            As soon as any player has something for each field, they can click "Stop!", which will stop the round for everyone and all players will be dropped into review.
          </DialogContentText>
          <DialogContentText variant="h8" color="black" style={{ marginTop: '10px' }}>
            The Review
          </DialogContentText>
          <DialogContentText style={{ marginTop: '10px' }}>
            Here, everyone can review the answers of everyone. If you think that the answer of another player is invalid, just click the "X" button next to this answer. If at least half of all players disagree with an answer, it will result in zero points. Also, make sure that answers that should be the same are written the same way. For this, just edit the answer in the text field.
          </DialogContentText>
          <DialogContentText style={{ marginTop: '10px' }}>
            As soon as you're happy with your settings, click the "I'm Ready" button on the bottom. As soon as all players have pressed this button, the points screen will pop up.
          </DialogContentText>
          <DialogContentText style={{ marginTop: '10px' }}>
            Ponits are awarded by the following rules:
            <ul>
              <li>No answer equals zero points.</li>
              <li>An answer with only one character also equals zero points.</li>
              <li>An answer with half the players disagreeing on also equals zero points.</li>
              <li>An answer which is shared with another player equals one point.</li>
              <li>An answer which noone else has thought of equals two points.</li>
              <li>An answer in a category noone else has answered equals three points.</li>
            </ul>
          </DialogContentText>
          <DialogContentText style={{ marginTop: '10px' }}>
            As soon as everyone has looked at the new points, click "Continue" to drop everyone into the next round.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.close} autoFocus>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

class MainDescription extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      openDialog: false,
    }
  }

  render() {
    return (
      <Box style={{ background: 'linear-gradient(180deg, ' + theme.palette.primary.dark + ' 0%, #ffffff 100%)', paddingTop: '50px', paddingBottom: '50px' }}>
        <Container>
          <IntroDialog openDialog={this.state.openDialog} close={() => this.setState({openDialog: false})}/>
          <Grid container spacing={2} alignItems="stretch">
            <Grid item xs={12} sm={6} md={7} style={{ display: 'flex' }}>
              <Card style={{display: 'flex', justifyContent: 'space-between', flexDirection: 'column'}}>
                <CardContent>
                  <Typography variant="h3" style={{
                    background: '-webkit-linear-gradient(40deg, ' + theme.palette.primary.dark + ', #D2B04C)',
                    WebkitBackgroundClip: 'text',
                    WebkitTextFillColor: 'transparent',
                  }}><b>Geography</b></Typography>
                  <Typography style={{ marginTop: '20px' }}>City with F? Food with P?</Typography>
                  <Typography style={{ marginTop: '20px' }}><i>Geography</i> is an online implementation of "Stadt, Land, Fluss": Start a new game, invite your friends with an invite link, define some categories and see who's most creative!</Typography>
                  <Typography style={{ marginTop: '20px' }}>Confused about how this works? Click the button below for an introduction!</Typography>
                </CardContent>
                <CardActions>
                  <Button
                    onClick={() => {this.setState({openDialog: true})}}
                  >
                    How does this work?
                  </Button>
                </CardActions>
              </Card>
            </Grid>
            <Grid item xs={12} sm={6} md={5} style={{ display: 'flex' }}>
              <Card style={{display: 'flex', justifyContent: 'space-between', flexDirection: 'column'}}>
                <CardContent>
                <Grid container spacing={3}>
                  <Grid item xs={12}>
                    <TextField
                      label="City"
                      value="Damascus"
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      label="Country"
                      value="Denmark"
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      label="River"
                      value="Dude, no idea"
                      fullWidth
                      autoFocus
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Button variant="contained">Stop!</Button>
                  </Grid>
                </Grid>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </Container>
      </Box>
    );
  }
}

export default MainDescription;
