import React from 'react';
import axios from 'axios';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
} from '@mui/material';

import colorsys from 'colorsys';

class AskName extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      buttonText: props.buttonText,
      allowClose: props.allowClose,
    }

    this.callback = props.callback.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.generateColor = this.generateColor.bind(this);
  }

  generateColor() {
    const S = 100;
    const V = 70;
    const H = Math.floor(Math.random() * 360);

    const col = colorsys.rgb2Hex(colorsys.hsv_to_rgb({h: H, s: S, v: V}));
    return col;
  }

  handleSubmit(e) {
    e.preventDefault(); 
    if(this.state.name != '') {
      axios.post('/api/login', {
        name: this.state.name,
        color: this.generateColor(),
      }).then(response => {
        const data = response.data;
        const obj = JSON.parse(data);
        this.callback(obj.name, obj.id);
      })
    }
  }

  render() {
    return (
      <Dialog
        open={this.props.dialogOpen}
        onClose={() => {this.state.allowClose && this.props.setDialogOpen(false)}}
      >
        <form>
          <DialogTitle>
            Enter your Name
          </DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              style={{ marginTop: '5px' }}
              label="Name"
              value={this.state.name}
              onChange={(e) => {this.setState({name: e.target.value})}}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={(e) => {this.handleSubmit(e)}} type="submit">{this.state.buttonText}</Button>
          </DialogActions>
        </form>
      </Dialog>
    );
  }
}

export default AskName;
