import React from 'react';
import * as ReactDOM from 'react-dom/client';
import axios from 'axios';

import {
  Alert,
  Avatar,
  Box,
  Button,
  ButtonGroup,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Chip,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  ListSubheader,
  Snackbar,
  TextField,
  Typography,
} from '@mui/material';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

import {
  Abc,
  Add,
  ContentCopy,
  Delete,
  Remove,
  Tag,
} from '@mui/icons-material';

import AskName from './AskName';
import Chat from './chat';

class Lobby extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      game: null,
      newField: '',
      player: null,
      dialogOpen: false,
      sb_open: false,
      sb_severity: 'success',
      sb_message: '',
    }

    this.getGame = this.getGame.bind(this);
    this.login = this.login.bind(this);
    this.loginCallback = this.loginCallback.bind(this);
    this.removeField = this.removeField.bind(this);
    this.removePlayer = this.removePlayer.bind(this);
    this.addField = this.addField.bind(this);
    this.getPlayer = this.getPlayer.bind(this);
    this.isMe = this.isMe.bind(this);
    this.amIHost = this.amIHost.bind(this);
    this.wasKicked = this.wasKicked.bind(this);
    this.startGame = this.startGame.bind(this);
    this.snackbar = this.snackbar.bind(this);
    this.getRandomLetter = this.getRandomLetter.bind(this);
  }

  componentDidMount() {
    this.getGame();
    this.login();

    const interval = setInterval(() => {
      this.getGame();
    }, 3000);

    const interval2 = setInterval(() => {
      this.login();
    }, 6000);
  }

  snackbar(severity, message) {
    this.setState({sb_open: true, sb_severity: severity, sb_message: message});
    return;
  }

  async getGame() {
    let split = window.location.href.split('/');
    const id = split[split.length-1];
    const response = await fetch('/api/games/' + id, {
      headers: {
        'accept': 'application/json'
      }
    });
    const data = await response.json();
    this.setState({
      game: data
    });

    // forward if game has started
    if(this.state.player != null && data.isStarted) {
      window.location = '/game/' + data.id;
    }
  }

  wasKicked(id) {
    // if session id is set, but the player is not in this game, the player was kicked 
    let result = true;
    if(this.state.game == null) {
      return false;
    }
    this.state.game.players.map(player => {
      if(player.id == id) {
        result = false;
      }
    });
    return result;
  }

  login() {
    axios.get('/api/user').then(response => {
      if(response.data == '' || this.wasKicked(response.data)) {
        this.setState({dialogOpen: true});
      } else {
        this.getPlayer(response.data);
      }
    });
  }

  getPlayer(id) {
    axios.get('/api/players/' + id).then(reponse => {
      this.setState({player: reponse.data});
    });
  }

  loginCallback(name, id) {
    axios.patch('/api/players/' + id, {
      game: '/api/games/' + this.state.game.id,
    }, {
      headers: {
        'Content-Type': 'application/merge-patch+json',
      }
    }).then(response => {
      this.setState({dialogOpen: false});
      this.getGame();
    });
  }

  removeField(id) {
    const response = axios.delete('/api/fields/' + id);
    this.getGame();
    return;
  }

  removePlayer(id) {
    const response = axios.delete('/api/players/' + id);
    this.getGame();
    return;
  }

  addField(e) {
    e.preventDefault();
    if(this.state.newField != '') {
      this.setState({newField: ''});
      const response = axios.post('/api/fields', {
        name: this.state.newField,
        game: '/api/games/' + this.state.game.id,
      }).then(() => {
        this.getGame();
      });
    } else {
      this.snackbar('error', "you'll have to enter a category");
    }
  }

  isMe(player) {
    return (this.state.player != null && this.state.player.id == player.id)
  }

  amIHost() {
    return (this.state.player != null && this.state.game != null && this.state.player.id == this.state.game.host.id); 
  }

  startGame() {
    axios.post('/api/rounds', {
      letter: this.getRandomLetter(),
      game: '/api/games/' + this.state.game.id,
      isFinished: false,
      isNextRound: false,
    }).then(() => {
      axios.patch('/api/games/' + this.state.game.id, {
        isStarted: true,
      }, {
        headers: {
          'Content-Type': 'application/merge-patch+json',
        }
      });
    });
  }

  getRandomLetter() {
    let choices = this.state.game.characterSet;
    let index = Math.floor(Math.random() * choices.length);
    return choices[index];
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <AskName dialogOpen={this.state.dialogOpen} buttonText="Join Game" callback={this.loginCallback} setDialogOpen={(v) => {this.setState({dialogOpen: v})}} allowClose={false}/>
        <Container>
          <Typography variant="h3" style={{ marginTop: '50px' }}>Lobby</Typography>

          <Grid container spacing={2} style={{ marginTop: '20px' }}>
            <Grid item xs={12} md={4}>
              <Card variant="outlined">
                <CardHeader title="Categories"/>
                <CardContent style={{ marginTop: '10px' }}>

                  {this.state.game != null && 
                  <List disablePadding>
                    {this.state.game.fields.map((field) => (
                      <ListItem key={field.id} secondaryAction={this.amIHost() && <IconButton edge="end" onClick={() => {this.removeField(field.id)}}><Delete/></IconButton>}>
                        <ListItemText primary={field.name}/>
                      </ListItem>
                      )
                    )}
                  </List>}
                  {this.amIHost() && <form>
                  <Grid container spacing={2} style={{ marginTop: '10px' }}>
                    <Grid item xs={9}>
                      <TextField
                        label="new Field"
                        value={this.state.newField}
                        onChange={(e) => {const w = e.target.value.charAt(0).toUpperCase() + e.target.value.slice(1); this.setState({newField: w})}}
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <Button type="submit" variant="contained" fullWidth style={{ height: "100%" }} onClick={(e) => {this.addField(e)}}>
                        Add Field
                      </Button>
                    </Grid>
                  </Grid>
                  </form>}
                </CardContent>
              </Card>

            </Grid>
            <Grid item xs={12} md={4}>
              {this.state.game != null && <Settings host={this.amIHost()} game={this.state.game} getGame={this.getGame} snackbar={this.snackbar}/>}
            </Grid>
            <Grid item xs={12} md={4}>
              <Card variant="outlined">
                <CardHeader title="Players"/>
                <CardContent>
                  <List>
                    {this.state.game != null && this.state.game.players.map(player => (
                      <ListItem key={player.id} disablePadding secondaryAction={this.amIHost() && player.id != this.state.game.host.id && <IconButton edge="end" onClick={() => {this.removePlayer(player.id)}}><Delete/></IconButton>}>
                        <ListItemButton>
                          <ListItemIcon>
                            <Avatar sx={{ bgcolor: player.color }}>{player.name.slice(0, 1)}</Avatar>
                          </ListItemIcon>
                          <ListItemText primary={player.name + (this.state.game.host.id == player.id ? " (Host)" : "") + (this.isMe(player) ? " (thas you)" : "")} />
                        </ListItemButton>
                      </ListItem>
                    ))}
                  </List>
                </CardContent>
                <CardActions>
                  {this.state.game != null &&
                  <Button
                    onClick={() => {navigator.clipboard.writeText('https://geography.aschoch.ch/lobby/' + this.state.game.id); this.snackbar('success', 'Copied!');}}
                    endIcon={<ContentCopy/>}
                  >
                    Copy invite Link
                  </Button>}
                </CardActions>
              </Card>
            </Grid>
          </Grid>
          
          <Box style={{ textAlign: 'center', marginTop: '50px' }}>
            {this.amIHost() ?
              <Button
                size="large"
                variant="contained"
                onClick={this.startGame}
              >
                {"Everybody's In"}
              </Button>
              : 
              <Alert severity="info">Please wait for the host to start the game</Alert>
            }
          </Box>

          {(this.state.game != null && this.state.player != null) && <Chat game={this.state.game} player={this.state.player}/>}
        </Container>
        <Snackbar open={this.state.sb_open} autoHideDuration={6000} onClose={() => {this.setState({sb_open: !this.state.sb_open})}}>
          <Alert onClose={() => {this.setState({sb_open: false})}} severity={this.state.sb_severity} sx={{ width: '100%' }}>
            {this.state.sb_message}
          </Alert>
        </Snackbar>
			</ThemeProvider>
    );
  }
}

class Settings extends React.Component {
  constructor(props) {
    super(props);

    this.updateNumRounds = this.updateNumRounds.bind(this);
    this.updateCharacterSet = this.updateCharacterSet.bind(this);
  }

  updateNumRounds(by) {
    if(this.props.game.numRounds + by <= this.props.game.characterSet.length) {
      axios.patch('/api/games/' + this.props.game.id, {
        numRounds: this.props.game.numRounds + by,
      }, {'headers': {
        'Content-Type': 'application/merge-patch+json',
      }}).then(() => {
        this.props.getGame();
      });
    } else {
      this.props.snackbar('error', 'you need more characters than rounds');
    }
  }

  updateCharacterSet(to) {
    if(to.length >= this.props.game.numRounds) {
      if(/(.).*\1/.test(to)) {
        this.props.snackbar('error', 'you cannot have duplicate letters');
      } else {
        axios.patch('/api/games/' + this.props.game.id, {
          characterSet: to,
        }, {'headers': {
          'Content-Type': 'application/merge-patch+json',
        }}).then(() => {
          this.props.getGame();
        });
      }
    } else {
      this.props.snackbar('error', 'you need more characters than rounds');
    }
  }

  render() {
    return (
     <Card variant="outlined">
        <CardHeader title="Settings"/>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              {this.props.host ?
                <List>
                  <ListItem>
                    <ListItemIcon>
                      <Tag />
                    </ListItemIcon>
                    <ListItemText primary={
                      <Box>
                        <ButtonGroup>
                          <IconButton onClick={() => this.updateNumRounds(1)}><Add/></IconButton>
                          <Button variant="text">{this.props.game.numRounds}</Button>
                          <IconButton onClick={() => this.updateNumRounds(-1)}><Remove/></IconButton>
                        </ButtonGroup>
                      </Box>
                    } />
                  </ListItem>
                  <ListItem style={{ overflow: 'scroll' }}>
                    <ListItemIcon>
                      <Abc />
                    </ListItemIcon>
                    <ListItemText primary={
                      <TextField
                        value={this.props.game.characterSet}
                        onChange={(e) => {this.updateCharacterSet(e.target.value.toUpperCase())}}
                        fullWidth
                      />
                    } />
                  </ListItem>
                </List>
              :
                <List>
                  <ListItem>
                    <ListItemIcon>
                      <Tag />
                    </ListItemIcon>
                    <ListItemText primary={this.props.game.numRounds} />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <Abc />
                    </ListItemIcon>
                    <ListItemText primary={this.props.game.characterSet} />
                  </ListItem>
                </List>
              }
            </Grid>
          </Grid>
        </CardContent>
     </Card>
    )
  }
}

const container = document.getElementById('lobby');
const root = ReactDOM.createRoot(container);
root.render(<Lobby/>);
