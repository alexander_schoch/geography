import React from 'react';
import * as ReactDOM from 'react-dom/client';
import axios from 'axios';

import {
  Button,
  Chip,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  Grid,
  TextField,
  Typography,
} from '@mui/material';

import LoadingButton from '@mui/lab/LoadingButton';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

class Game extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      game: null,
      player: null,
      round: null,
      answers: [],
      stop: false,
      letter: '',
      ready: false,
    }

    this.getGame = this.getGame.bind(this);
    this.getPlayer = this.getPlayer.bind(this);
    this.prepare = this.prepare.bind(this);
    this.sync = this.sync.bind(this);
    this.isFinished = this.isFinished.bind(this);
    this.amIDone = this.amIDone.bind(this);
    this.stop = this.stop.bind(this);
    this.getFieldValue = this.getFieldValue.bind(this);
  }

  componentDidMount() {
    const promise1 = this.getGame();
    const promise2 = this.getPlayer();
    Promise.all([promise1, promise2]).then(() => {
      this.prepare().then(() => {
        setInterval(this.isFinished, 1000);
      });
    });
  }

  async getPlayer() {
    const response = await axios.get('/api/user');
    if(response.data == '') {
      window.location = '/lobby/' + this.state.game.id;
    } else {
      const response2 = await axios.get('/api/players/' + response.data)
      return new Promise((resolve) => {
        this.setState({player: response2.data}, () => {
          resolve();
        });
      });
    }
  }

  async prepare() {
    // DONE: this has to change
    let obj = [];
    this.state.game.fields.map(field => {
      obj.push({
        field: field.id,
        text: '',
      });
    });
    return new Promise((resolve) => {
      this.setState({
        answers: obj,
        ready: true,
      }, () => {
        resolve();
      });
    });
  }

  async getGame() {
    let split = window.location.href.split('/');
    const id = split[split.length-1];
    const response = await axios.get('/api/games/' + id);
    return new Promise((resolve) => {
      this.setState({
        game: response.data,
        letter: response.data.rounds[response.data.progress].letter,
      }, () => {
        resolve();
      });
    });
  }

  async sync() {
    // DONE: this should now send all answers to an api
    const response = await axios.post('/api/saveAnswers', {
      answers: this.state.answers,
      player: this.state.player.id,
      round: this.state.game.rounds[this.state.game.progress].id,
    });
    return new Promise((resolve) => {
      console.log(response);
      resolve();
    });
  }

  write(e, field_passed) { // e is an event, field_passed is a field id
    // DONE: the data structure for answers will change
    let obj = [];
    if(e.target.value.length != 0 && e.target.value[0].toUpperCase() != this.state.letter) {
      return;
    }
    this.state.answers.map(answer => {
      if(field_passed != answer.field) {
        obj.push(answer); 
      } else {
        obj.push({'field': answer.field, 'text': e.target.value.charAt(0).toUpperCase() + e.target.value.slice(1)}); 
      }
    });
    this.setState({answers: obj});
  }

  async isFinished() {
    const response = await axios.get('/api/finished/' + this.state.game.rounds[this.state.game.progress].id); 
    if(response.data) {
      this.setState({stop: true});
      const r = await this.sync();
      window.location = '/discuss/' + this.state.game.id;
    }   
  }

  amIDone() {
    // DONE: answers have a new data structure
    let done = true;
    this.state.answers.map(answer => {
      if(answer.text.length < 2) {
        done = false;
      }
    });
    return done;
  }

  stop(e) {
    e.preventDefault();
    this.setState({stop: true});
    axios.patch('/api/rounds/' + this.state.game.rounds[this.state.game.progress].id, {
      isFinished: true,
    }, {
      headers: {
        'Content-Type': 'application/merge-patch+json',
      }
    });
  }

  getFieldValue(field) {
    // DONE: this will probably change too
    let ans = null;
    this.state.answers.map(a => {
      if(a.field == field.id) {
        ans = a;
      }
    });
    if(ans != null) {
      return ans.text;
    } else {
      return "";
    }
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        {this.state.ready && 
        <Container>
          <Typography variant="h3" style={{ marginTop: '50px' }}>Round {this.state.game.progress + 1} / {this.state.game.numRounds}</Typography>
          <Divider style={{ marginTop: '30px', marginBottom: '30px' }}>
            <Chip color="primary" variant="outlined" label={<span style={{ fontSize: '15pt' }}>Letter: <b>{this.state.letter}</b></span>} />
          </Divider>
          <form>
            <Grid container spacing={2} style={{ marginTop: '20px' }}>
            {this.state.ready && this.state.game.fields.map((field, index) => (
              <Grid item xs={12} md={6} key={field.id}>
                <TextField
                  autoFocus={index == 0}
                  label={field.name}
                  value={this.getFieldValue(field)}
                  onChange={(e) => {this.write(e, field.id)}}
                  disabled={this.state.stop}
                  fullWidth
                />
              </Grid>
            ))}
            </Grid>
            <Grid container spacing={2} style={{ marginTop: '10px' }}>
              <Grid item xs={12}>
                <LoadingButton
                  variant="contained"
                  onClick={(e) => this.stop(e)}
                  disabled={!this.amIDone()}
                  type="submit"
                  loading={this.state.stop}
                >
                  Stop!
                </LoadingButton>
              </Grid>
            </Grid>
          </form>
        </Container>}
			</ThemeProvider>
    );
  }
}

const container = document.getElementById('game');
const root = ReactDOM.createRoot(container);
root.render(<Game/>);
