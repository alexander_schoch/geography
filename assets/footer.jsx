import React from 'react';
import * as ReactDOM from 'react-dom/client';

import {
	Box,
	Container,
	Grid,
	Typography,
} from '@mui/material';

import { ThemeProvider } from '@mui/material/styles';
import theme from './theme';

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render () {
    return (
			<ThemeProvider theme={theme}>
				<Box style={{ backgroundColor: theme.palette.primary.dark, paddingTop: '50px', paddingBottom: '50px', marginTop: '50px', color: 'white' }}>
					<Container>
						<Grid container>
							<Grid item xs={12} md={6}>
								<Typography variant="h5" color="primary.light">	
									About
								</Typography>	
								<Typography color="primary.light">	
									&#169; Alexander Schoch, 2022<br/>
									<a href="mailto:schochal@student.ethz.ch" style={{ color: '#F0EDCC' }}>schochal@student.ethz.ch</a><br/>
								 	CH – 8049 Zürich<br/>
								</Typography>	
							</Grid>	
							<Grid item xs={12} md={6}>
								<Typography variant="h5" style={{ textAlign: window.innerWidth > 1000 ? 'right' : 'left', marginTop: window.innerWidth > 1000? '0px' : '20px' }} color="primary.light">	
									Bugs? Problems?
								</Typography>	
								<Typography style={{ textAlign: window.innerWidth > 1000 ? 'right' : 'left' }} color="primary.light">	
									Contact Me: <a href="mailto:schochal@student.ethz.ch" style={{ color: '#F0EDCC' }}>schochal@student.ethz.ch</a><br/>
								</Typography>	
								<Typography variant="h5" style={{ textAlign: window.innerWidth > 1000 ? 'right' : 'left', marginTop: '20px' }} color="primary.light">	
									Open Source
								</Typography>	
								<Typography style={{ textAlign: window.innerWidth > 1000 ? 'right' : 'left' }} color="primary.light">	
									The Source code of this application can be found on <a href="https://gitlab.com/alexander_schoch/znacht" style={{ color: '#F0EDCC' }}>https://gitlab.com/alexander_schoch/znacht</a> and is distributed under the GNU AGPL 3.0.
								</Typography>	
							</Grid>	
						</Grid>
					</Container>
				</Box>
			</ThemeProvider>
    )
  }
}


const container = document.getElementById('footer');
const root = ReactDOM.createRoot(container);
root.render(<Footer/>);
