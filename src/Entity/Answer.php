<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\AnswerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AnswerRepository::class)]
#[ApiResource(
  normalizationContext: ['groups' => ['answer']],
)]
class Answer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['round', 'answer'])]
    private ?string $answer = null;

    #[ORM\ManyToOne(inversedBy: 'answers')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['round', 'answer'])]
    private ?Player $player = null;

    #[ORM\ManyToOne(inversedBy: 'answers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Round $round = null;

    #[ORM\ManyToOne(inversedBy: 'answers')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['round', 'answer'])]
    private ?Field $field = null;

    #[ORM\OneToMany(mappedBy: 'answer', targetEntity: Disapproval::class, orphanRemoval: true)]
    #[Groups('round')]
    private Collection $disapprovals;

    public function __construct()
    {
        $this->disapprovals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(?string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getRound(): ?Round
    {
        return $this->round;
    }

    public function setRound(?Round $round): self
    {
        $this->round = $round;

        return $this;
    }

    public function getField(): ?Field
    {
        return $this->field;
    }

    public function setField(?Field $field): self
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return Collection<int, Disapproval>
     */
    public function getDisapprovals(): Collection
    {
        return $this->disapprovals;
    }

    public function addDisapproval(Disapproval $disapproval): self
    {
        if (!$this->disapprovals->contains($disapproval)) {
            $this->disapprovals->add($disapproval);
            $disapproval->setAnswer($this);
        }

        return $this;
    }

    public function removeDisapproval(Disapproval $disapproval): self
    {
        if ($this->disapprovals->removeElement($disapproval)) {
            // set the owning side to null (unless already changed)
            if ($disapproval->getAnswer() === $this) {
                $disapproval->setAnswer(null);
            }
        }

        return $this;
    }
}
