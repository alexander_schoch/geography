<?php

namespace App\Controller;

use App\Repository\PlayerRepository;

use Doctrine\Persistence\ManagerRegistry;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DiscussController extends AbstractController
{
    #[Route('/discuss/{id}', name: 'discuss')]
    public function index(int $id): Response
    {
        return $this->render('discuss/index.html.twig');
    }

    #[Route('/api/addPoints/{id}', name: 'addPoints')]
    public function addPoints(int $id, Request $request, PlayerRepository $pr, ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();

        $data = $request->getContent();
        $data = json_decode($data);

        $player = $pr->findOneById($id);
        $player->setPoints($player->getPoints() + $data->points);

        $em->persist($player);
        $em->flush();

        return new Response('OK');
    }
}
