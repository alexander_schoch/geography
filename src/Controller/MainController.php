<?php

namespace App\Controller;

use App\Repository\AnswerRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class MainController extends AbstractController
{
    #[Route('/', name: 'main')]
    public function main(): Response
    {
        return $this->render('main/index.html.twig');
    }

    #[Route('/api/randomAnswer', name: 'randomAnswer')]
    public function randomAnswer(AnswerRepository $ar): Response
    {
        $limits = $ar->findOneAtRandom();
        if($limits == [1 => null, 2 => null]) {
          return new Response(0);  
        }
        $iterations = 0;
        do {
          $rand = \random_int($limits[1], $limits[2]);
          $a = $ar->findOneById($rand);
          $iterations += 1;
        } while (strlen($a->getAnswer() < 3) && $iterations < 10);

        return new Response($a->getId());
    }
}
