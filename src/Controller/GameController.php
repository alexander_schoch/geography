<?php

namespace App\Controller;

use App\Entity\Answer;
use App\Repository\AnswerRepository;
use App\Repository\FieldRepository;
use App\Repository\GameRepository;
use App\Repository\PlayerRepository;
use App\Repository\RoundRepository;

use Doctrine\Persistence\ManagerRegistry;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends AbstractController
{
    #[Route('/game/{id}', name: 'game')]
    public function index(int $id): Response
    {
        return $this->render('game/index.html.twig');
    }

    #[Route('/api/finished/{id}', name: 'finished')]
    public function finished(int $id, RoundRepository $rr): Response
    {
        $r = $rr->findOneById($id);
        return new Response($r->isIsFinished() ? 'true' : 'false'); 
    }

    #[Route('/api/saveAnswers', name: 'saveAnswers')]
    public function saveAnswers(Request $request, FieldRepository $fr, GameRepository $gr, PlayerRepository $pr, RoundRepository $rr, AnswerRepository $ar, ManagerRegistry $doctrine): Response
    {
        $em = $doctrine->getManager();

        $data = $request->getContent();
        $data = json_decode($data);

        $round_id = $data->round;
        $player_id = $data->player;
        $answers = $data->answers;

        $round = $rr->findOneById($round_id);
        $player = $pr->findOneById($player_id);

        foreach($answers as $answer) {
          $field = $fr->findOneById($answer->field);
          $existingAnswer = $ar->findOneByPlayerRoundAndField($player, $round, $field);
          if($existingAnswer == null) {
            $a = new Answer();
            $a->setPlayer($player);
            $a->setRound($round);
            $a->setField($field);
            $a->setAnswer($answer->text);

            $em->persist($a);
          }
        }

        $em->flush();
        return new Response('OK');
    }
}
