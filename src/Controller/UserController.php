<?php

namespace App\Controller;

use App\Entity\Player;

use Doctrine\Persistence\ManagerRegistry;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/api/login', name: 'login_api')]
    public function loginApi(Request $request, ManagerRegistry $doctrine)
    {
        $em = $doctrine->getManager();

        $data = $request->getContent();
        $data = json_decode($data);

        $player = new Player();
        $player->setName($data->name);
        $player->setColor($data->color);
        $player->setIsReady(false);
        $player->setPoints(0);

        $em->persist($player);
        $em->flush();

        $session = $request->getSession();

        $session->set('name', $player->getName());
        $session->set('id', $player->getId());

        return new JsonResponse('{"name": "' . $player->getName() . '", "id": "' . $player->getId() . '"}');
    }

    #[Route('/api/user', name: 'user_api')]
    public function userApi(Request $request, ManagerRegistry $doctrine)
    {
        $session = $request->getSession();

        $name = $session->get('name');
        $id = $session->get('id');

        return new Response($id);
    }
}
