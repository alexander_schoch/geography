#!/usr/bin/env bash

php bin/console doctrine:migrations:migrate

chmod -R 777 /var/www/geography/var/cache
chmod -R 777 /var/www/geography/var/log

nginx
php-fpm
