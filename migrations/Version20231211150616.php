<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231211150616 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE disapproval (id INT AUTO_INCREMENT NOT NULL, player_id INT NOT NULL, answer_id INT NOT NULL, INDEX IDX_1C94CEBE99E6F5DF (player_id), INDEX IDX_1C94CEBEAA334807 (answer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, game_id INT NOT NULL, player_id INT NOT NULL, text VARCHAR(255) NOT NULL, INDEX IDX_B6BD307FE48FD905 (game_id), INDEX IDX_B6BD307F99E6F5DF (player_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE disapproval ADD CONSTRAINT FK_1C94CEBE99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE disapproval ADD CONSTRAINT FK_1C94CEBEAA334807 FOREIGN KEY (answer_id) REFERENCES answer (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FE48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE answer ADD CONSTRAINT FK_DADD4A2599E6F5DF FOREIGN KEY (player_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE answer ADD CONSTRAINT FK_DADD4A25A6005CA0 FOREIGN KEY (round_id) REFERENCES round (id)');
        $this->addSql('ALTER TABLE answer ADD CONSTRAINT FK_DADD4A25443707B0 FOREIGN KEY (field_id) REFERENCES field (id)');
        $this->addSql('ALTER TABLE field ADD CONSTRAINT FK_5BF54558E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE game ADD is_started TINYINT(1) NOT NULL, ADD num_rounds INT NOT NULL, ADD progress INT NOT NULL, ADD character_set VARCHAR(255) NOT NULL, DROP rounds');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C1FB8D185 FOREIGN KEY (host_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE player ADD color VARCHAR(255) NOT NULL, ADD is_ready TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE player ADD CONSTRAINT FK_98197A65E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE round ADD game_id INT NOT NULL, ADD is_finished TINYINT(1) NOT NULL, ADD is_next_round TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE round ADD CONSTRAINT FK_C5EEEA34E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('CREATE INDEX IDX_C5EEEA34E48FD905 ON round (game_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE disapproval DROP FOREIGN KEY FK_1C94CEBE99E6F5DF');
        $this->addSql('ALTER TABLE disapproval DROP FOREIGN KEY FK_1C94CEBEAA334807');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FE48FD905');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F99E6F5DF');
        $this->addSql('DROP TABLE disapproval');
        $this->addSql('DROP TABLE message');
        $this->addSql('ALTER TABLE answer DROP FOREIGN KEY FK_DADD4A2599E6F5DF');
        $this->addSql('ALTER TABLE answer DROP FOREIGN KEY FK_DADD4A25A6005CA0');
        $this->addSql('ALTER TABLE answer DROP FOREIGN KEY FK_DADD4A25443707B0');
        $this->addSql('ALTER TABLE round DROP FOREIGN KEY FK_C5EEEA34E48FD905');
        $this->addSql('DROP INDEX IDX_C5EEEA34E48FD905 ON round');
        $this->addSql('ALTER TABLE round DROP game_id, DROP is_finished, DROP is_next_round');
        $this->addSql('ALTER TABLE player DROP FOREIGN KEY FK_98197A65E48FD905');
        $this->addSql('ALTER TABLE player DROP color, DROP is_ready');
        $this->addSql('ALTER TABLE field DROP FOREIGN KEY FK_5BF54558E48FD905');
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318C1FB8D185');
        $this->addSql('ALTER TABLE game ADD rounds INT DEFAULT NULL, DROP is_started, DROP num_rounds, DROP progress, DROP character_set');
    }
}
